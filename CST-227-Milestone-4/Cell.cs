﻿//Cell class that extends button, William Thornton, CST-227, Milestone-5, Coded by William Thornton on 08/17/2019
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CST_227_Milestone_4
{
    public class Cell : Button
    {
        int row = -1;
        int column = -1;
        bool visited = false;
        bool live = false;
        int neighbors = 0;

        public Cell(int r, int c)
        {
            row = r;
            column = c;
        }

        //setting the size of the buttons, so they look square like minesweeper tiles
        protected override Size DefaultSize
        {
            get
            {
                return new Size(40, 40);
            }
        }

        //get functions
        public bool getVisited()
        {
            return visited;
        }

        public bool getLive()
        {
            return live;
        }
        public int getNeighbors()
        {
            return neighbors;
        }

        public int getRow()
        {
            return row;
        }

        public int getColumn()
        {
            return column;
        }

        //set functions

        public void setVisited(bool v)
        {
            visited = v;
        }

        public void setLive(bool l)
        {
            live = l;
        }
        public void setNeighbors(int n)
        {
            neighbors = n;
        }

        public void setRow(int r)
        {
            row = r;
        }

        public void setColumn(int c)
        {
            column = c;
        }


    }
}
